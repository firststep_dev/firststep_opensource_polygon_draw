window.onload = function() {
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    var polygons = [];
    var currentPoints = [];
    var selectedColor = 'red';
    var isDrawing = false;

    canvas.onclick = function(e) {
        if (isDrawing) {
            var rect = canvas.getBoundingClientRect();
            var x = e.clientX - rect.left;
            var y = e.clientY - rect.top;
            var point = { x: x, y: y };

            if (currentPoints.length > 0 && Math.abs(x - currentPoints[0].x) < 10 && Math.abs(y - currentPoints[0].y) < 10) {
                finishPolygon();
            } else {
                currentPoints.push(point);
            }
            redraw();
        }
    };

    function redraw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        polygons.forEach(function(polygon) {
            drawPolygon(polygon.points, polygon.color);
        });

        if (currentPoints.length > 0) {
            drawPolygon(currentPoints, selectedColor, false);
        }
    }

    function drawPolygon(points, color, isComplete = true) {

        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.arc(points[0].x, points[0].y, 3, 0, 2 * Math.PI);
        ctx.fill();

        if (points.length < 2) return;

        for (var i = 1; i < points.length; i++) {
            ctx.fillStyle = color;
            ctx.beginPath();
            ctx.arc(points[i].x, points[i].y, 3, 0, 2 * Math.PI);
            ctx.fill();
        }

        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.fillStyle = getTransparentColor(color, 0.2);
        ctx.moveTo(points[0].x, points[0].y);
        for (var i = 1; i < points.length; i++) {
            ctx.lineTo(points[i].x, points[i].y);
        }
        if (isComplete) {
            ctx.closePath();
            ctx.fill();
        }
        ctx.stroke();
    }

    function getTransparentColor(color, opacity) {
        var colors = { red: [255, 0, 0], blue: [0, 0, 255], green: [0, 128, 0] };
        var rgb = colors[color];
        return 'rgba(' + rgb.join(',') + ',' + opacity + ')';
    }

    function finishPolygon() {
        polygons.push({ points: currentPoints, color: selectedColor });
        addRowToTable(selectedColor, currentPoints);
        currentPoints = [];
        isDrawing = false;
        document.getElementById('colorSelect').disabled = false;
    }

    function addRowToTable(color, points) {
        var table = document.getElementById('polygonTable');
        var row = table.insertRow();
        var colorCell = row.insertCell(0);
        var pointsCell = row.insertCell(1);
        var deleteCell = row.insertCell(2);
        var deleteButton = document.createElement('button');

        colorCell.textContent = color;
        pointsCell.textContent = points.map(p => `(${Math.round(p.x)},${Math.round(p.y)})`).join('; ');
        deleteButton.textContent = 'Delete';
        deleteButton.onclick = function() {
            // Identify the polygon to delete
            for (var i = 0; i < polygons.length; i++) {
                if (polygons[i].color === color && JSON.stringify(polygons[i].points) === JSON.stringify(points)) {
                    polygons.splice(i, 1);
                    table.deleteRow(row.rowIndex);
                    break;
                }
            }
            redraw();
        };
        deleteCell.appendChild(deleteButton);
    }

    document.getElementById('colorSelect').onchange = function(e) {
        if (!isDrawing) {
            selectedColor = e.target.value;
        }
    };

    document.getElementById('startPolygon').onclick = function() {
        isDrawing = true;
        document.getElementById('colorSelect').disabled = true;
    };
};
